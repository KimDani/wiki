KimDani's Wiki
================================
:math:`\\frac{ \\sum_{t=0}^{N}f(t,k) }{N}`


Contents:

.. toctree::
   :maxdepth: 2
   Using_Forum/index
   Using_Sphinx/index
   Using_Git/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
