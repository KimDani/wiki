HCP data 기술문서
===============
본 문서는 NEUROPHET에서 보유하고 있는 HCP(Human Connectome Project) 데이터에 대한 설명을 하는 문서입니다.

1. HCP 데이터란?
---------------
HCP(Human Connectome Project)는 `NIC Blueprint for Neuroscience Research`__ 프로젝트중 하나로, 수백명의 인간의 뇌영상 (MRI, fMRI, DTI, MEG)을 촬영하고 이를 웹상에 공유하여 뇌공학 연구를 가속화 시킨 프로젝트입니다.
HCP데이터를 생산하고 있는 연구기관은 Harvard/MGH-UCLA_ 연합과 WU-Minn_ 연합이 가지고 있는데 데이터로 구분이 됩니다.
NEUROPHET에서는 WU-Minn 연합이 축적하고 있는 데이터에서 인간의 뇌의 구조적 정보를 담고 있는 MRI데이터와 이를 freesurfer_ 로 segmentation한 결과를 보관하고 있습니다 (900 Subjects).

.. __: https://www.neuroscienceblueprint.nih.gov/connectome/
.. _freesurfer: http://freesurfer.net/
.. _MGH-UCLA: http://www.humanconnectomeproject.org/
.. _WU-Minn: http://humanconnectome.org/

2. HCP 데이터 접근방법 (2016. 11. 01)
------------------------------------
해당 데이터가 상당히 크기 때문에(~1TB), 회사에 NAS가 셋팅되기 이전까지는 개인하드로 보관하고 있습니다. 이중 T1-MRI와 이를 segementation한 결과를 서버컴퓨터의 FTP에 보관하고 있습니다. (ftp://neurophet.iptime.org)
FTP 서버로 로그인 후 /HCP 폴더를 접속해 보면 T1-MRI와 segmented label이 .nii 포맷으로 저장되어 있습니다.
만약 MGZ포멧(용량이 적은 포멧이지만 윈도우에서 읽을 수 없음)이 필요하다면 /HCP/MGZ 폴더에 저장되어 있습니다.

2. HCP 데이터 사용방법 (2016. 11. 01)
------------------------------------
해당 데이터를 가시적으로 보기 위해서는 freesurfer_ (for linux user)를 설치하거나 3DSlicer_ (for windows user)를 설치하여 볼 수 있습니다.
데이터셋의 이름은 segmented label의 경우 LABEL_SUBJECTNUMBER.nii 이며, T1-MRI의 경우 MRI_SUBJECTNUMBER.nii 입니다.
segmented label의 경우 freesurfer의 aseg+aparc.mgz를 .nii로 변환한 것이기 때문에 freesurfer LUT_ 에서 찾아볼 수 있지만 너무 많은 라벨이 설정되어 있고, 해당 라벨을 모두 구분하는 것은 전기자극 시뮬레이션을 위한 뇌모델에서는 필요없기 때문에 자체 LUT를 제작하여 수정하길 권장합니다.

**권장하는 라벨 Lookup table (아래로 갈수록 우선순위가 높음)**

====================== =============== ================
Original Label          Tissue          Modified Label
====================== =============== ================
Other                   None            1
10000 - 30000           Cerebral GM     2
1 - 9999                Cerebral WM     3
4 5 14 24 31 43 44 63   Ventricles      4
7 8 14 46 47            Cerebellum      5
====================== =============== ================

.. _freesurfer: http://freesurfer.net/
.. _3DSlicer: https://www.slicer.org/
.. _LUT: https://surfer.nmr.mgh.harvard.edu/fswiki/FsTutorial/AnatomicalROI/FreeSurferColorLUT

2. MGZ, MGH, NII 파일 파이썬으로 읽기
------------------------------------
해당하는 포맷은 모두 biomedical format으로 3차원 영상데이터 입니다. 해당포멧은 python의 nibabel_ 으로 읽을 수 있으며, 아래는 데이터를 읽는 예제입니다.

::

 import nibabel as nib
  with open(filename,"r") as f:
    Label_data = nib.load(filename)

.. _nibabel: http://nipy.org/nibabel/
